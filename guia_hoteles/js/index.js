$(function(){
    $("[data-bs-toggle='tooltip']").tooltip();
    $("[data-bs-toggle='popover']").popover();
    $(".carousel").carousel({
      interval: 2000
    });
  });

  $("#modalContacto").on("show.bs.modal", function(e){
    console.log("Se está ejecutando el show del modal");
    let boton = e.relatedTarget;
    $(boton).removeClass("btn-outline-success");
    $(boton).addClass("btn-primary");
    $(boton).prop("disabled", true);
  });

  $("#modalContacto").on("shown.bs.modal", function(e){
    console.log("Se está ejecutando el shown del modal");
  });

  $("#modalContacto").on("hide.bs.modal", function(e){
    console.log("Se está ejecutando el hide del modal");
  });

  $("#modalContacto").on("hidden.bs.modal", function(e){
    console.log("Se está ejecutando el hidden del modal");
    $("[data-bs-toggle='modal']").removeClass("btn-primary");
    $("[data-bs-toggle='modal']").addClass("btn-outline-success");        
    $("[data-bs-toggle='modal']").prop("disabled", false);
  });